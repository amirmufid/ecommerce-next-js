/** @type {import('next').NextConfig} */
const { i18n } = require('./next-i18next.config')


const nextConfig = {
  reactStrictMode: true,
  i18n,
  images: {
    // loader: 'custom',
    // loaderFile: './node_modules/@uploadcare/nextjs-loader/build/loader.js',
    remotePatterns: [
      {
        protocol: 'http',
        hostname: 'localhost',
        port: '3000',
        pathname: '/assets/img/**',
      },
    ],
  },

}

module.exports = nextConfig;
