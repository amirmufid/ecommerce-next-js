export const CURRENCY_OPTION = [{
  value : 'usd',
  label : '$ USD'
},{
  value : 'idr',
  label : 'Rp. IDR'
},{
  value : 'jpy',
  label : '¥ JPY'
}]

export const LANGUAGE_OPTION = [{
  value : 'en',
  label : 'English',
  flag_img : '/assets/img/flags/fr.png' 
},{
  value : 'id',
  label : 'Bahasa Indonesia',
  flag_img : '/assets/img/flags/fr.png'
},{
  value : 'jp',
  label : 'Japanese',
  flag_img : '/assets/img/flags/fr.png'
}]
