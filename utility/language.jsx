import { useTranslation } from 'next-i18next'

export const useLanguage = (nameSpace='common') => {
  const { t, i18n } = useTranslation(nameSpace);
  
  switch(typeof nameSpace){
      case 'string' :
          return {
              lang : (key)=>t(key),
              i18n
          };
      case 'object' :
          return {
              lang: (key,ns='common')=>t(key, {ns}),
              i18n
          };
      default : 
          return {
              lang : (key)=>t(key),
              i18n
          };
  }
}
