// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

export default function handler(req, res) {
  const data = [{
    id : 1,
    name : 'Computers & Accessories',
    parrent_id : 0,
    image : 'http://localhost:3000/assets/img/shop/departments/07.jpg',
    icon : 'ci-laptop'
  },{
    id : 2,
    name : 'Computers',
    parrent_id : 1,
    image : null,
    icon : null
  },{
    id : 3,
    name : 'Laptops & Tablets',
    parrent_id : 2,
    image : null,
    icon : null
  },{
    id : 4,
    name : 'Desktop & Computers',
    parrent_id : 2,
    image : null,
    icon : null
  }]
  res.status(200).json(data);
}
