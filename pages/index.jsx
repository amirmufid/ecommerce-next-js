
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { useLanguage } from '../utility/language';

import Layout from "../components/Layout";
import Banner from '../components/Banner';

import ProductCardLists from '../components/Products/ProductCardLists';
import SpecialOfferProduct from '../components/Products/SpecialOfferProduct';
import Img from '../components/Img';

export default function Index() {
  const { lang, i18n } = useLanguage();

  return (
    <Layout title={lang('meta.title')} description={lang('meta.description')}>
      
      <section className="bg-secondary py-4 pt-md-5">
        <div className="container py-xl-2">
          <Banner />
        </div>
      </section>

      <section className="container my-4">
        <div className="d-flex flex-wrap justify-content-between align-items-center pt-1 border-bottom pb-4 mb-4">
          <h2 className="h3 mb-0 pt-3 me-2">Flash Sales</h2>
          <div className="pt-3"><a className="btn btn-outline-accent btn-sm" href="shop-grid-ls.html">More products<i className="ci-arrow-right ms-1 me-n1"></i></a></div>
        </div>
        <SpecialOfferProduct />
        <ProductCardLists data={[1,2,3,4,5,6,7,8,9,10]} column={5} carousel={true} />
      </section>

      <section className="container my-4">
        <div className="row">
          <div className="col-md-8 mb-4">
            <div className="d-sm-flex justify-content-between align-items-center bg-secondary overflow-hidden rounded-3">
              <div className="py-4 my-2 my-md-0 py-md-5 px-4 ms-md-3 text-center text-sm-start">
                <h4 className="fs-lg fw-light mb-2">Hurry up! Limited time offer</h4>
                <h3 className="mb-4">Converse All Star on Sale</h3><a className="btn btn-primary btn-shadow btn-sm" href="#">Shop Now</a>
              </div>
              <div className='image-container'>
                <Img className="d-block ms-auto" src="/assets/img/shop/catalog/banner.jpg" placeholder="blur" alt="Shop Converse" fill sizes="100" />
              </div>
              
            </div>
          </div>
          <div className="col-md-4 mb-4">
            <div className="d-flex flex-column h-100 justify-content-center bg-size-cover bg-position-center rounded-3" style={{'backgroundImage': 'url(assets/img/blog/banner-bg.jpg)'}}>
              <div className="py-4 my-2 px-4 text-center">
                <div className="py-1">
                  <h5 className="mb-2">Your Add Banner Here</h5>
                  <p className="fs-sm text-muted">Hurry up to reserve your spot</p><a className="btn btn-primary btn-shadow btn-sm" href="#">Contact us</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      
      <section className="container my-4">
        <div className="d-flex flex-wrap justify-content-between align-items-center pt-1 border-bottom pb-4 mb-4">
          <h2 className="h3 mb-0 pt-3 me-2">Trending Products</h2>
          <div className="pt-3"><a className="btn btn-outline-accent btn-sm" href="shop-grid-ls.html">More products<i className="ci-arrow-right ms-1 me-n1"></i></a></div>
        </div>
        <ProductCardLists data={[1,2,3,4,5,6,7,8]} column={4} />
      </section>

    </Layout>
  );
}

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['common','login_form'])),
    },
  }
}
