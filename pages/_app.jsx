import '../styles/globals.css'

import { wrapper } from '../store/store'
import { Provider } from 'react-redux'
import { appWithTranslation } from 'next-i18next'
import Script from 'next/script'

// Import our custom CSS
import '../public/assets/vendor/simplebar/dist/simplebar.min.css'
import '../public/assets/vendor/tiny-slider/dist/tiny-slider.css'
import '../public/assets/vendor/drift-zoom/dist/drift-basic.min.css'
import '../public/assets/css/theme.min.css'

const App = ({ Component, pageProps, ...rest }) => {
  const {store, props} = wrapper.useWrappedStore(rest);
  return (
    <Provider store={store}>
    <Script src="/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js" strategy="afterInteractive"/>
    <Script src="/assets/vendor/simplebar/dist/simplebar.min.js" strategy="afterInteractive"/>
    <Script src="/assets/vendor/tiny-slider/dist/min/tiny-slider.js" strategy="afterInteractive"/>
    <Script src="/assets/vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js" strategy="afterInteractive"/>
    <Script src="/assets/vendor/drift-zoom/dist/Drift.min.js" strategy="afterInteractive"/>
      <Component {...pageProps} />
    </Provider>
  )
}



export default appWithTranslation(App)
