module.exports = {
  i18n: {
    // ATTENTION: The default locale does not have a prefix.
    defaultLocale: 'id',
    locales: ['en', 'id', 'jp'],
  },
  fallbackLng: {
    default: ['en']
  },
  nonExplicitSupportedLngs: true,
}
