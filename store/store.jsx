import { combineReducers, configureStore } from '@reduxjs/toolkit'
import { createWrapper, HYDRATE } from 'next-redux-wrapper'

//RTK State
import { ProductCategoriesApi } from './services/ProductCategories'

//Redux State
import counter from './features/counter/counterSlice'

const combineReducer = combineReducers({
  counter,
  [ProductCategoriesApi.reducerPath] : ProductCategoriesApi.reducer
})

const masterReducer = (state, action) => {
  let nextState
  if(action.type === HYDRATE){
     nextState = {
      ...state
    }
  } else {
    nextState = combineReducer(state, action)
  }

  return nextState;
}

export const makeStore = () => configureStore({
  reducer: masterReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({ serializableCheck: false, })
    .concat(ProductCategoriesApi.middleware)
  ,
})

export const wrapper = createWrapper(makeStore,{debug:true});
