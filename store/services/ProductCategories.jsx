import { serialize } from "../../utility"
import {Api} from "./Api"

export const ProductCategoriesApi = Api.injectEndpoints({
    reducerPath: 'product_categories',
    endpoints: (builder) => ({
        getProductCategories: builder.query({
            query: (params) => ({url: `product_categories?${serialize(params)}`}),
            providesTags: ['ProductCategories']
        }),
        addProductCategories: builder.mutation({
            query: (body) => ({url: `product_categories`, method: 'POST', body}),
            invalidatesTags: ['ProductCategories']
        })
    })
})

export const {useGetProductCategoriesQuery, useAddProductCategoriesMutation} = ProductCategoriesApi
