import Head from "next/head";
import Header from "./Header";
import Script from 'next/script';
import Footer from "./Footer";


const Layout = ({children, title, description}) => {
  return (
    <>
    <Head>
      <meta charSet="UTF-8" />
      <title>{`eCommerce - ${title}`}</title>

      {/* SEO META TAGS */}
      <meta name="description" content={description} />
      <meta name="robots" content="all" />
      <meta name="author" content="eCommerce" />

      {/* Viewport */}
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    </Head>
    <Header />

    <div>{children}</div>
    <Footer />
    <Script src="/assets/js/theme.min.js" strategy="lazyOnload" />
    </>
  );
}

export default Layout;
