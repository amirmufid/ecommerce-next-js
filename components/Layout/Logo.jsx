import Img from "../Img";

const Logo = () => {
  return (
    <div>
      <a className="navbar-brand d-none d-sm-block me-3 flex-shrink-0" href="#">
        <Img src="/assets/img/logo-dark.png" alt="Cartzilla" placeholder="blur" width="142" height={40}/>
      </a>
      <a className="navbar-brand d-sm-none me-2" href="#">
        <Img src="/assets/img/logo-icon.png" alt="Cartzilla" placeholder="blur" width="74"  height={40}/>
      </a>
    </div>
  );
}

export default Logo;
