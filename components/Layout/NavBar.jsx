import { useLanguage } from "../../utility/language";
import Img from "../Img";
import { useGetProductCategoriesQuery } from "../../store/services/ProductCategories";

const NavBar = () => {
  const {lang, i18n} = useLanguage()
  const {isSuccess,isError,isFetching,data} = useGetProductCategoriesQuery();
  
  const renderMegaMenu = (data,parrent=null,master=null) => {
    let ren = []
    
    const filterData = data?.filter((x)=> x.parrent_id === parrent)
    if(parrent == 0) {
      filterData?.map((item)=>{
        ren.push(
          <li key={item.id} className="dropdown mega-dropdown">
              <a className="dropdown-item dropdown-toggle" href="#" data-bs-toggle="dropdown">
                <i className={`${item.icon} opacity-60 fs-lg mt-n1 me-2`}></i>{item.name}
              </a>
              <div className="dropdown-menu p-0">
                  <div className="d-flex flex-wrap flex-sm-nowrap px-2">
                    
                      <div className="row pt-4 pb-0 py-sm-4 px-3" style={{width:'30rem'}}>
                      {renderMegaMenu(data,item.id,item.id)}
                      </div>
                      
                      <div className="mega-dropdown-column d-none d-lg-block py-4 text-center">
                          <a className="d-block mb-2" href="#">
                            <div className="image-container">
                              <Img src={item.image} placeholder="blur" alt="Computers &amp; Accessories" fill sizes="100"/>
                            </div>
                          </a>
                      </div>
                  </div>
              </div>
          </li>
        )
      })
    } else {
      filterData?.map((item)=>{
        const index = data.findIndex((z)=>z.parrent_id == item.id)
        if(index >= 0) {
          ren.push(
            <div key={item.id} className="widget widget-links col-6 md-3">
              <h6 className="fs-base mb-3">{item.name}</h6>
              <ul className="widget-list">
                {renderMegaMenu(data,item.id,master)}
              </ul>
            </div>
          )
        } else {
          if(master == item.parrent_id){
            
          } else {
            ren.push(<li key={item.id} className="widget-list-item pb-1"><a className="widget-list-link" href="#">{item.name}</a></li>)
          }
        }
      
      })
    }
    return ren
  }

  return (
    <div className="navbar-sticky bg-light">
      <div className="navbar navbar-expand-lg navbar-light navbar-stuck-menu mt-n2 pt-0 pb-2">
        <div className="container">
          <div className="collapse navbar-collapse" id="navbarCollapse">
            
            <div className="input-group d-lg-none my-3"><i className="ci-search position-absolute top-50 start-0 translate-middle-y ms-3"></i>
              <input className="form-control rounded-start" type="text" placeholder="Search for products" />
            </div>
            
            <ul className="navbar-nav navbar-mega-nav pe-lg-2 me-lg-2">
              <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle ps-lg-0" href="#" data-bs-toggle="dropdown" data-bs-auto-close="outside">
                  <i className="ci-menu align-middle mt-n1 me-2"></i>{lang('layout.nav.categories')}
                </a>
                <ul className="dropdown-menu">
                  {isSuccess && (
                    renderMegaMenu(data,0,0)
                  )}  
                  
                </ul>
              </li>
            </ul>
            
            <ul className="navbar-nav">
              <li className="nav-item"><a className="nav-link" href="#">{lang('layout.nav.get_mobile_apps')}</a></li>
              <li className="nav-item"><a className="nav-link" href="#">{lang('layout.nav.tracking_order')}</a></li>
              <li className="nav-item"><a className="nav-link" href="#">{lang('layout.nav.blog')}</a></li>
              <li className="nav-item dropdown"><a className="nav-link dropdown-toggle" href="#" data-bs-toggle="dropdown" data-bs-auto-close="outside">{lang('layout.nav.guide')}</a>
                <ul className="dropdown-menu">
                  <li><a className="dropdown-item" href="about.html">{lang('layout.nav.for_buyers')}</a></li>
                  <li><a className="dropdown-item" href="contacts.html">{lang('layout.nav.for_sellers')}</a></li>
                </ul>
              </li>
              
              <li className="nav-item dropdown"><a className="nav-link dropdown-toggle" href="#" data-bs-toggle="dropdown">{lang('layout.nav.customer_care')}</a>
                <ul className="dropdown-menu">
                  <li>
                    <a className="dropdown-item" href="docs/dev-setup.html">
                      <div className="d-flex">
                        <div className="lead text-muted pt-1"><i className="ci-book"></i></div>
                        <div className="ms-2" style={{lineHeight:'2rem'}}><span className="d-block text-heading">{lang('layout.nav.help_center')}</span></div>
                      </div>
                    </a>
                  </li>
                  <li className="dropdown-divider"></li>
                  <li>
                    <a className="dropdown-item" href="docs/dev-setup.html">
                      <div className="d-flex">
                        <div className="lead text-muted pt-1"><i className="ci-book"></i></div>
                        <div className="ms-2" style={{lineHeight:'2rem'}}><span className="d-block text-heading">{lang('layout.nav.order_n_payment')}</span></div>
                      </div>
                    </a>
                  </li>
                  <li className="dropdown-divider"></li>
                  <li>
                    <a className="dropdown-item" href="docs/dev-setup.html">
                      <div className="d-flex">
                        <div className="lead text-muted pt-1"><i className="ci-book"></i></div>
                        <div className="ms-2" style={{lineHeight:'2rem'}}><span className="d-block text-heading">{lang('layout.nav.canceling_order')}</span></div>
                      </div>
                    </a>
                  </li>
                  <li className="dropdown-divider"></li>
                  <li>
                    <a className="dropdown-item" href="docs/dev-setup.html">
                      <div className="d-flex">
                        <div className="lead text-muted pt-1"><i className="ci-book"></i></div>
                        <div className="ms-2" style={{lineHeight:'2rem'}}><span className="d-block text-heading">{lang('layout.nav.delivery_order')}</span></div>
                      </div>
                    </a>
                  </li>
                  <li className="dropdown-divider"></li>
                  <li>
                    <a className="dropdown-item" href="docs/dev-setup.html">
                      <div className="d-flex">
                        <div className="lead text-muted pt-1"><i className="ci-book"></i></div>
                        <div className="ms-2" style={{lineHeight:'2rem'}}><span className="d-block text-heading">{lang('layout.nav.refund')}</span></div>
                      </div>
                    </a>
                  </li>
                  <li className="dropdown-divider"></li>
                  <li>
                    <a className="dropdown-item" href="docs/dev-setup.html">
                      <div className="d-flex">
                        <div className="lead text-muted pt-1"><i className="ci-server"></i></div>
                        <div className="ms-2"><span className="d-block text-heading">{lang('layout.nav.contact_us')}</span><small className="d-block text-muted">{lang('layout.nav.live_chat_24h')}</small></div>
                      </div>
                    </a>
                  </li>
                </ul>
              </li>

            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}

export default NavBar;
