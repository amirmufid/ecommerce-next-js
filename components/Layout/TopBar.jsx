import Img from "../Img";

import { CURRENCY_OPTION, LANGUAGE_OPTION } from "../../constans";
import { useLanguage } from "../../utility/language";

const TopBar = () => {
  const {lang, i18n} = useLanguage()
  
  return (
    <div className="topbar topbar-dark bg-dark">
        <div className="container">
            <div className="topbar-text text-nowrap d-md-inline-block">
                <i className="ci-support"></i>
                <span className="text-muted me-1">{lang('layout.topbar.support')}</span>
                <a className="topbar-link" href="tel:00331697720">(00) 33 169 7720</a>
            </div>
            <div className="topbar-text dropdown disable-autohide">
                <a className="topbar-link dropdown-toggle" href="#" data-bs-toggle="dropdown"><Img className="me-2" src="/assets/img/flags/en.png" width="20" height="1" alt="English" />Eng / $</a>
                <ul className="dropdown-menu dropdown-menu-end">
                  <li>
                    <a className="dropdown-item pb-1" href="#"><Img className="me-2" src="/assets/img/flags/fr.png" width="20" height="1" alt="Français" />Français</a>
                  </li>
                  <li>
                    <a className="dropdown-item pb-1" href="#"><Img className="me-2" src="/assets/img/flags/de.png" width="20" height="1" alt="Deutsch" />Deutsch</a>
                  </li>
                  <li>
                      <a className="dropdown-item" href="#"><Img className="me-2" src="/assets/img/flags/it.png" width="20" height="1" alt="Italiano" />Italiano</a>
                  </li>
                </ul>
            </div>
        </div>
    </div>
  );
}

export default TopBar;
