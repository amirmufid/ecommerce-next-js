import { useLanguage } from "../../utility/language";
import TopBar from "./TopBar";
import NavBar from "./NavBar";
import Logo from "./Logo";
import MyAccount from "./MyAccount";


const Header = () => {
  const {lang, i18n} = useLanguage()
  return (
    <div className="handheld-toolbar-enabled">
      <main className="page-wrapper">
      
        <header className="shadow-sm">
          <TopBar />
          <div className="navbar navbar-expand-lg navbar-light">
            <div className="container">

              <Logo />
              
              <div className="input-group d-none d-lg-flex flex-nowrap mx-4"><i className="ci-search position-absolute top-50 start-0 translate-middle-y ms-3"></i>
                <input className="form-control rounded-start w-100" type="text" placeholder={lang('layout.header.search_for_product')} />
              </div>
              
              <MyAccount />

            </div>
          </div>
          <NavBar />
        </header>

      </main>
    </div>
  );
}

export default Header;
