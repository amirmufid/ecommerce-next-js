import { useState } from "react";
import { useLanguage } from "../../utility/language";
import Img from "../Img";
import LoginModal from "../Login/LoginModal"

const MyAccount = () => {
  const {lang, i18n} = useLanguage()
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <>
      <div className="navbar-toolbar d-flex flex-shrink-0 align-items-center">
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse"><span className="navbar-toggler-icon"></span></button>
        <a className="navbar-tool navbar-stuck-toggler" href="#">
          <div className="navbar-tool-icon-box"><i className="navbar-tool-icon ci-menu"></i></div>
        </a>
        <a onClick={handleShow} className="navbar-tool ms-1 ms-lg-0 me-n1 me-lg-2">
          <div className="navbar-tool-icon-box"><i className="navbar-tool-icon ci-user"></i></div>
          {/* <div className="navbar-tool-text ms-n3"><small>Hello</small>Amir Mufid</div> */}
          <div className="navbar-tool-text ms-n3">Login / Register</div>
        </a>
        <div className="navbar-tool dropdown ms-3">
          <a className="navbar-tool-icon-box bg-secondary dropdown-toggle" href="shop-cart.html">
            <span className="navbar-tool-label">4</span>
            <i className="navbar-tool-icon ci-cart"></i>
          </a>
          <a className="navbar-tool-text" href="shop-cart.html">
            <small>{lang('layout.header.my_cart')}</small>$1,247.00
          </a>

          <div className="dropdown-menu dropdown-menu-end">
            <div className="widget widget-cart px-3 pt-2 pb-3" style={{'width': '20rem'}}>
              <div style={{'height': '15rem'}} data-simplebar data-simplebar-auto-hide="false">
                <div className="widget-cart-item pb-2 border-bottom">
                  <button className="btn-close text-danger" type="button" aria-label="Remove"><span aria-hidden="true">&times;</span></button>
                  <div className="d-flex align-items-center"><a className="d-block flex-shrink-0" href="shop-single-v2.html">
                    <Img src="/assets/img/shop/cart/widget/05.jpg" width="64" height={1} alt="Product" /></a>
                    <div className="ps-2">
                      <h6 className="widget-product-title"><a href="shop-single-v2.html">Bluetooth Headphones</a></h6>
                      <div className="widget-product-meta"><span className="text-accent me-2">$259.<small>00</small></span><span className="text-muted">x 1</span></div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="d-flex flex-wrap justify-content-between align-items-center py-3">
                <div className="fs-sm me-2 py-2"><span className="text-muted">{lang('layout.header.subtotal')}:</span><span className="text-accent fs-base ms-1">$1,247.<small>00</small></span></div><a className="btn btn-outline-secondary btn-sm" href="shop-cart.html">{lang('layout.header.expand_cart')}<i className="ci-arrow-right ms-1 me-n1"></i></a>
              </div><a className="btn btn-primary btn-sm d-block w-100" href="checkout-details.html"><i className="ci-card me-2 fs-base align-middle"></i>{lang('layout.header.checkout')}</a>
            </div>
          </div>

        </div>
      </div>
      <LoginModal show={show} handleClose={handleClose} />
    </>
    
  );
}

export default MyAccount;
