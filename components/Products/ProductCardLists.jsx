import Device from "../Device"
import ProductItem from "./ProductItem"
import ProductCardCarousel from "./ProductCardCarousel"

const ProductCardLists = ({data,column,carousel=false}) => {
  
  return (
    <Device>
      {({ isMobileOnly, isTablet }) => {  
        if(isTablet){
          column = 4
        } else if(isMobileOnly){
          column = 2
        }

        if(carousel){
          return <ProductCardCarousel data={data} column={column}/>
        }

        return (
          <div className="row mx-n2">
            {data.map((x)=>(
              <ProductItem column={column} key={x}/>
            ))}
          </div>
        )
      }}
    </Device>
  )
  
}

export default ProductCardLists;
