import Img from "../Img";

const ProductItem = ({column}) => {
  let columnClass, columnStyle
  switch (column) {
    case 1:
      columnClass = 'col-12'
      break;
    case 2:
      columnClass = 'col-6'
      break;
    case 3:
      columnClass = 'col-4'
      break;
    case 4:
      columnClass = 'col-3'
      break;
    case 5:
      columnClass = 'col-2'
      columnStyle = {'width':'19.666667%'}
      break;
    case 6:
      columnClass = 'col-2'
      break;
    default:
      columnClass = 'col'
      break;
  }

  return (
    <div className={`${columnClass} px-2 mb-4`} style={columnStyle}>
      <div className="card product-card card-static">
        <div className="product-card-actions d-flex align-items-center">
          <button className="btn-wishlist btn-sm" type="button" data-bs-toggle="tooltip" data-bs-placement="left" title="Add to wishlist"><i className="ci-heart"></i></button>
        </div>
        <a className="card-img-top d-block overflow-hidden" href="shop-single-v2.html">
          <div className="image-container">
            
          <Img src="/assets/img/shop/catalog/58.jpg" placeholder="blur" fill sizes="100" alt="Product" />
          </div>
        </a>
        <div className="card-body py-2"><a className="product-meta d-block fs-xs pb-1" href="#">Headphones</a>
          <h3 className="product-title fs-sm"><a href="shop-single-v2.html">Wireless Bluetooth Headphones</a></h3>
          <div className="d-flex justify-content-between">
            <div className="product-price"><span className="text-accent">$198.<small>00</small></span></div>
            <div className="star-rating"><i className="star-rating-icon ci-star-filled active"></i><i className="star-rating-icon ci-star-filled active"></i><i className="star-rating-icon ci-star-filled active"></i><i className="star-rating-icon ci-star-filled active"></i><i className="star-rating-icon ci-star-filled active"></i>
            </div>
          </div>
        </div>
        <div className="card-body card-body-hidden">
          <button className="btn btn-primary btn-sm d-block w-100 mb-2" type="button"><i className="ci-cart fs-sm me-1"></i>Add to Cart</button>
        </div>
      </div>
      <hr className="d-sm-none" />
    </div>
  );
}

export default ProductItem;
