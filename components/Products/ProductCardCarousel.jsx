import ProductItem from "./ProductItem";

const ProductCardCarousel = ({data,column}) => {
  let item = []
  column = column || 6
  for (let i = 0; i < data.length; i += column) {
      const chunk = data.slice(i, i + column);
      item.push(
        <div key={i}>
          <div className="row mx-n2">    
            {chunk.map((x)=>(<ProductItem column={column} key={x}/>))}
          </div>
        </div>
      )
  }
  
  return (
    <div className="tns-carousel" >
      <div className="tns-carousel-inner" data-carousel-options="{&quot;nav&quot;: false, &quot;controlsContainer&quot;: &quot;#hoodie-day&quot;}">
        {item}
      </div>
      
      <div className="tns-carousel-controls" id="hoodie-day">
        <button type="button"><i className="ci-arrow-left"></i></button>
        <button type="button"><i className="ci-arrow-right"></i></button>
      </div> 
    </div>
  )
}

export default ProductCardCarousel;
