import { useLanguage } from "../../utility/language";
import { Modal } from "react-bootstrap";
import FormLogin from "./FormLogin";
import FormRegister from "./FormRegister";

const LoginModal = ({show,handleClose}) => {
    const {lang, i18n} = useLanguage('login_form')
       
    return (
        <>
        <Modal 
            show={show} onHide={handleClose} centered>
            <Modal.Header className="bg-secondary" closeButton>
                <ul className="nav nav-tabs card-header-tabs" role="tablist">
                    <li className="nav-item">
                        <a
                            className="nav-link fw-medium active"
                            href="#signin-tab"
                            data-bs-toggle="tab"
                            role="tab"
                            aria-selected="true">
                            <i className="ci-unlocked me-2 mt-n1"></i>{lang('tab.sign_in')}</a>
                    </li>
                    <li className="nav-item">
                        <a
                            className="nav-link fw-medium"
                            href="#signup-tab"
                            data-bs-toggle="tab"
                            role="tab"
                            aria-selected="false">
                            <i className="ci-user me-2 mt-n1"></i>{lang('tab.sign_up')}</a>
                    </li>
                </ul>
            </Modal.Header>
            <Modal.Body className="tab-content py-4">
                <FormLogin />
                <FormRegister />
            </Modal.Body>
            <Modal.Footer>
                
            </Modal.Footer>
        </Modal>
        </>
    )
}

export default LoginModal;
