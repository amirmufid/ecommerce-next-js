import { useLanguage } from "../../utility/language";
import { useFormik } from "formik";
import * as Yup from "yup"

const FormLogin = () => {
    const {lang, i18n} = useLanguage('login_form')
    const FormikSignIn = useFormik({
        initialValues: {
            username: '',
            password: '',
            remember_me: false
        },
        validationSchema: Yup.object().shape({
            username: Yup.string()
                .min(3,lang('sign_in.validation.username.min').replace('%NUM%','3'))
                .required(lang('sign_in.validation.username.required')),
            password: Yup.string()
                .min(3,lang('sign_in.validation.password.min').replace('%NUM%','3'))
                .required(lang('sign_in.validation.password.required')),
            remember_me: Yup.boolean()
        }),
        onSubmit: values => {
            console.log(values,'FormikSignIn');
        }
    });

    return (
        <form
            className="tab-pane fade show active"
            autoComplete="off"
            noValidate="noValidate"
            id="signin-tab"
            onSubmit={FormikSignIn.handleSubmit}
        >
            <div className="mb-3">
                <label className="form-label" htmlFor="si-email">{lang('sign_in.label.username')}</label>
                <input
                    className={`form-control ${FormikSignIn.errors.username && FormikSignIn.touched.username ? `is-invalid` : null}`}
                    type="text"
                    id="sign-in-username"
                    placeholder={lang('sign_in.placeholder.username')}
                    {...FormikSignIn.getFieldProps('username')}
                />
                {FormikSignIn.errors.username && FormikSignIn.touched.username && (<i className="invalid-feedback">{FormikSignIn.errors.username}</i>)}
            </div>
            <div className="mb-3">
                <label className="form-label" htmlFor="si-password">{lang('sign_in.label.password')}</label>
                <div className="password-toggle">
                    <input
                        className={`form-control ${FormikSignIn.errors.password && FormikSignIn.touched.password ? `is-invalid` : null}`}
                        type="password"
                        id="sign-in-password"
                        {...FormikSignIn.getFieldProps('password')}
                    />
                    <label className="password-toggle-btn" aria-label="Show/hide password">
                        <input className="password-toggle-check" type="checkbox"/>
                        <span className="password-toggle-indicator"></span>
                    </label>
                    {FormikSignIn.errors.password && FormikSignIn.touched.password && (<i className="invalid-feedback">{FormikSignIn.errors.password}</i>)}
                </div>
            
            </div>
            <div className="mb-3 d-flex flex-wrap justify-content-between">
                <div className="form-check mb-2">
                    <input 
                        className="form-check-input" 
                        type="checkbox" 
                        id="remember_me"
                        {...FormikSignIn.getFieldProps('remember_me')}
                    />
                    <label className="form-check-label" htmlFor="si-remember">{lang('sign_in.remember_me')}</label>
                </div>
                <a className="fs-sm" href="#">{lang('sign_in.forgot_password')}</a>
            </div>
            <button className="btn btn-primary btn-shadow d-block w-100" type="submit">{lang('sign_in.btn.submit')}</button>
        </form>
    )
}

export default FormLogin;
