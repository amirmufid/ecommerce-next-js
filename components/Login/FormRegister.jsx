import { useLanguage } from "../../utility/language";
import { useFormik } from "formik";
import * as Yup from "yup"

const FormRegister = () => {
    const {lang, i18n} = useLanguage('login_form')
    const FormikSignUp = useFormik({
      initialValues: {
        first_name: '',
        last_name: '',
        email: '',
        password: '',
        confirm_password: ''
      },
      validationSchema: Yup.object().shape({
        first_name: Yup.string()
          .required(lang('sign_up.validation.first_name.required')),
        last_name: Yup.string()
          .required(lang('sign_up.validation.last_name.required')),
        email: Yup.string()
          .email(lang('sign_up.validation.email.email'))
          .required(lang('sign_up.validation.email.required')),
        password: Yup.string()
          .min(3,lang('sign_up.validation.password.min').replace('%NUM%','3'))
          .required(lang('sign_up.validation.password.required')),
        confirm_password: Yup.string()
          .required(lang('sign_up.validation.confirm_password.required'))
          .oneOf([Yup.ref('password'), null], lang('sign_up.validation.confirm_password.match'))
      }),
      onSubmit: values => {
          console.log(values,'FormikSignUp');
      }
  });

    return (
      <form
          className="tab-pane fade"
          autoComplete="off"
          noValidate="noValidate"
          id="signup-tab"
          onSubmit={FormikSignUp.handleSubmit}
      >

        <div className="mb-3">
            <label className="form-label" htmlFor="sign-up-fist_name">{lang('sign_up.label.first_name')}</label>
            <input
                className={`form-control ${FormikSignUp.errors.first_name && FormikSignUp.touched.first_name ? `is-invalid` : FormikSignUp.values.first_name && `is-valid`}`}
                type="text"
                id="sign-up-fist_name"
                placeholder={lang('sign_up.placeholder.first_name')}
                {...FormikSignUp.getFieldProps('first_name')}
              />
              {FormikSignUp.errors.first_name && FormikSignUp.touched.first_name && (<i className="invalid-feedback">{FormikSignUp.errors.first_name}</i>)}
        </div>

        <div className="mb-3">
            <label className="form-label" htmlFor="sign-up-last_name">{lang('sign_up.label.last_name')}</label>
            <input
                className={`form-control ${FormikSignUp.errors.last_name && FormikSignUp.touched.last_name ? `is-invalid` : FormikSignUp.values.last_name && `is-valid`}`}
                type="text"
                id="sign-up-last_name"
                placeholder={lang('sign_up.placeholder.last_name')}
                {...FormikSignUp.getFieldProps('last_name')}
              />
              {FormikSignUp.errors.last_name && FormikSignUp.touched.last_name && (<i className="invalid-feedback">{FormikSignUp.errors.last_name}</i>)}
        </div>

        <div className="mb-3">
            <label htmlFor="sign-up-email">{lang('sign_up.label.email')}</label>
            <input
                className={`form-control ${FormikSignUp.errors.email && FormikSignUp.touched.email ? `is-invalid` : FormikSignUp.values.email && `is-valid`}`}
                type="email"
                id="sign-up-email"
                placeholder={lang('sign_up.placeholder.email')}
                {...FormikSignUp.getFieldProps('email')}
              />
              {FormikSignUp.errors.email && FormikSignUp.touched.email && (<i className="invalid-feedback">{FormikSignUp.errors.email}</i>)}
        </div>

        <div className="mb-3">
            <label className="form-label" htmlFor="sign-up-password">{lang('sign_up.label.password')}</label>
            <div className="password-toggle">
                <input
                  className={`form-control ${FormikSignUp.errors.password && FormikSignUp.touched.password ? `is-invalid` : FormikSignUp.values.password && `is-valid`}`}
                  type="password"
                  id="sign-up-password"
                  {...FormikSignUp.getFieldProps('password')}
                />
                <label className="password-toggle-btn" aria-label="Show/hide password">
                    <input className="password-toggle-check" type="checkbox"/>
                    <span className="password-toggle-indicator"></span>
                </label>
                {FormikSignUp.errors.password && FormikSignUp.touched.password && (<i className="invalid-feedback">{FormikSignUp.errors.password}</i>)}
            </div>
        </div>

        <div className="mb-3">
            <label className="form-label" htmlFor="sign-up-confirm_password">{lang('sign_up.label.confirm_password')}</label>
            <div className="password-toggle">
                <input
                  className={`form-control ${FormikSignUp.errors.confirm_password && FormikSignUp.touched.confirm_password ? `is-invalid` : FormikSignUp.values.confirm_password && `is-valid`}`}
                  type="password"
                  id="sign-up-confirm_password"
                  {...FormikSignUp.getFieldProps('confirm_password')}
                />
                <label className="password-toggle-btn" aria-label="Show/hide password">
                    <input className="password-toggle-check" type="checkbox"/>
                    <span className="password-toggle-indicator"></span>
                </label>
                {FormikSignUp.errors.confirm_password && FormikSignUp.touched.confirm_password && (<i className="invalid-feedback">{FormikSignUp.errors.confirm_password}</i>)}
            </div>
        </div>

        <button className="btn btn-primary btn-shadow d-block w-100" type="submit">{lang('sign_up.btn.submit')}</button>
      </form>
    )
}

export default FormRegister;
