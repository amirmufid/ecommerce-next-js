
import { useState } from "react";
import Image from "next/image";

const Img = ({alt, ...props}) => {
  const [src, setSrc] = useState(props.src);
  let blurSrc = null
  if(props?.placeholder === 'blur'){
    blurSrc = "/assets/img/image-placeholder.png"
  }
  return (
    <Image
      {...props}
      src={src}
      alt={alt} // To fix lint warning 
      onError={() => setSrc('/assets/img/image-error.jpg')}
      blurDataURL={blurSrc}
    />
  );
}

export default Img;
