import Img from "../Img";

const Banner = () => {
  return (
    <div className="row">
      
      <div className="col-xl-9 pt-xl-4 order-xl-2">
        <div className="tns-carousel">
          <div className="tns-carousel-inner" data-carousel-options="{&quot;items&quot;: 1, &quot;controls&quot;: false, &quot;loop&quot;: false}">
            
            <BannerItem src="/assets/img/home/hero-slider/04.jpg">
              <h2 className=" from-start delay-1">
                Explore the best <br />
                VR Collection <br />
                on the market
              </h2>
            </BannerItem>

          </div>
        </div>
      </div>

      <div className="col-xl-3 order-xl-1 pt-4 mt-3 mt-xl-0 pt-xl-0">
        <div className="table-responsive" data-simplebar>
          <div className="d-flex d-xl-block">
          
          <BannerGroupItem src="/assets/img/home/banners/banner-sm01.png">
            <h5 className="mb-2">
              Next Gen
              <br />
              Video with
              <br />
              360 Cam
            </h5>
          </BannerGroupItem>

          </div>
        </div>
      </div>


    </div>
  );
}

const BannerItem = ({src,children}) => {
  return (
    <div>
      <div className="row align-items-center">
        <div className="col-md-6 order-md-2">
        <div className="image-container">
          <Img src={src} alt="VR Collection" className="d-block mx-auto" fill sizes="100" placeholder="blur" priority={true}/>
          </div>
        </div>
        <div className="col-lg-5 col-md-6 offset-lg-1 order-md-1 pt-4 pb-md-4 text-center text-md-start">
            {children}
            <div className="d-table scale-up delay-4 mx-auto mx-md-0">
              <a className="btn btn-primary btn-shadow" href="shop-grid-ls.html">Shop Now<i className="ci-arrow-right ms-2 me-n1"></i></a>
            </div>
        </div>
      </div>
    </div>
  )
}

const BannerGroupItem = ({src,children}) => {
  return (
    <div className="d-flex align-items-center bg-faded-info rounded-3 pt-2 ps-2 mb-4 me-4 me-xl-0" style={{'minWidth': '16rem'}}>
      <Img src={src} width="125" height="1" alt="Banner" priority={true}/>
      <div className="py-4 px-2">
        {children}
        <div className="text-info fs-sm">Shop now<i className="ci-arrow-right fs-xs ms-1"></i></div>
      </div>
    </div>
  )
}

export default Banner;
